from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list": todo_lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    lists = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": lists,
    }
    return render(request, "todos/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save(False)
            list.author = request.user
            list.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm()
    context = {
        "form" : form,
    }
    return render(request, "todos/create.html", context)

def update_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance = list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListForm(instance = list)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def delete_todo_list(request, id):
    todos_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todos_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item.author = request.user
            item.save()
            return redirect("todo_list_detail", id=item.list.id )

    else:
        form = TodoItemForm()
    context = {
        "form" : form,
    }
    return render(request, "todos/items/create.html", context)

def update_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItem(request.POST, instance = item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoItemForm(instance = item)
    context = {
        "form": form,
    }
    return render(request, "todos/items/edit.html", context)
